import pandas as pd

from validation import validate_ip_port
from rtsp_client import RTSPClient

# Парсим все камеры с порт
data = pd.read_csv('https://raw.githubusercontent.com/CamioCam/rtsp/master/cameras/paths.csv')
rtsp_urls = data[data['rtsp_url'].str.contains("{{port}}")]['rtsp_url']

working_ports = []
working_all_cameras = []


def get_rtsp_in_template(username, password, ip, port, stream='', channel=''):
    """Заменяем значения камер на пользовательские"""
    for rtsp_url in rtsp_urls:
        rtsp_url = rtsp_url.replace("{{", "{").replace('}}', '}')
        rtsp_url = rtsp_url.format(username=username, password=password, ip_address=ip, port=port, stream=stream,
                                   channel=channel)
        config = {
            "ip": ip,
            "port": port,
            "url": rtsp_url,
            "login": username,
            "password": password
        }
        # Проверка ip и port
        if not validate_ip_port(ip, port):
            return False
        # Запрос к камере
        if RTSPClient(config=config).send_play_request():
            return rtsp_url
    return False


def get_all_working_ports(ip):
    """Находим все порты с камерами"""
    for change_port in range(500, 601):
        port = change_port
        if validate_ip_port(ip, port):
            working_ports.append(port)
            print(port)
    return working_ports


def get_all_working_cameras(working_ports, rtsp_url, replace_port):
    """Получаем все камеры"""
    for port in working_ports:
        working_all_cameras.append(rtsp_url.replace(replace_port, str(port)))
    return working_all_cameras


def start(username, password, ip, port):
    rtsp_url = get_rtsp_in_template(username, password, ip, port)
    if not rtsp_url:
        return 'No correct data'
    working_ports = get_all_working_ports(ip)
    return get_all_working_cameras(working_ports=working_ports, rtsp_url=rtsp_url, replace_port=str(port))
