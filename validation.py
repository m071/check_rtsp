import socket


def validate_ip_port(ip, port):
    """ Проверка ip и порта """
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(0.2)
    result = s.connect_ex((ip, port))
    if result == 0:
        return True
    else:
        return False
