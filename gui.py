import tkinter as tk
from tkinter import messagebox

from main import start


class RTSPInterface:
    def __init__(self, win):
        self.win = win
        self.win.title("get cameras")
        self.win.geometry("300x400")

        self.username_label = tk.Label(self.win, text="Username:")
        self.username_entry = tk.Entry(self.win)
        self.username_label.grid(row=0, column=0)
        self.username_entry.grid(row=0, column=1)

        self.password_label = tk.Label(self.win, text="Password:")
        self.password_entry = tk.Entry(self.win, show="*")
        self.password_label.grid(row=1, column=0)
        self.password_entry.grid(row=1, column=1)

        self.ip_label = tk.Label(self.win, text="IP Address:")
        self.ip_entry = tk.Entry(self.win)
        self.ip_label.grid(row=2, column=0)
        self.ip_entry.grid(row=2, column=1)

        self.port_label = tk.Label(self.win, text="Port:")
        self.port_entry = tk.Entry(self.win)
        self.port_label.grid(row=3, column=0)
        self.port_entry.grid(row=3, column=1)

        self.start_button = tk.Button(self.win, text="Start", command=self.start)
        self.start_button.grid(row=4, column=0, columnspan=2, pady=10)

        self.result_label = tk.Label(self.win, text='')
        self.result_label.grid(row=9, column=0, columnspan=2, padx=5, pady=5)

    def start(self):
        username = self.username_entry.get()
        password = self.password_entry.get()
        ip = self.ip_entry.get()
        port = self.port_entry.get()

        if not all([username, password, ip, port]):
            messagebox.showerror('Fill in all fields')
            return

        result = start(username=username, password=password, ip=ip, port=int(port))
        self.result_label.config(text='\n'.join(result))


if __name__ == '__main__':
    win = tk.Tk()
    RTSPInterface(win)
    win.mainloop()
