import base64
import socket


class RTSPClient:
    def __init__(self, config):
        self.config = config
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((self.config['ip'], self.config['port']))

    def authorization(self, request, config):
        if 'login' in config and 'password' in config:
            auth = base64.b64encode(
                (config['login'] + ':' + config['password']).encode()
            ).decode()
            request += "Authorization: Basic " + auth + "\r\n"
        return request

    def send_options_request(self):
        request = (
                "OPTIONS " + self.config['url'] + " RTSP/1.0\r\n" +
                "CSeq: 1"  "\r\n" +
                "User-Agent: Python MJPEG Client"  "\r\n"
        )
        request = self.authorization(request, self.config)
        request += "\r\n"
        self.s.sendall(request.encode())
        response = self.s.recv(4096).decode()
        if "RTSP/1.0 200 OK" in response:
            return True
        else:
            return False

    def send_describe_request(self):
        if self.send_options_request():
            request = (
                    "DESCRIBE " + self.config['url'] + " RTSP/1.0\r\n" +
                    "CSeq: 2"  "\r\n" +
                    "Accept: application/sdp\r\n" +
                    "User-Agent: Python MJPEG Client "  "\r\n"
            )
            request = self.authorization(request, self.config)
            request += "\r\n"
            self.s.sendall(request.encode())
            response = self.s.recv(4096).decode()
            print(response)
            if "RTSP/1.0 200 OK" in response:
                return True
            else:
                return False

    def send_setup_request(self):
        if self.send_describe_request():
            request = (
                    "SETUP " + self.config['url'] + '/trackID=1' + " RTSP/1.0\r\n" +
                    "CSeq: 3"  "\r\n" +
                    "User-Agent: Python MJPEG Client"  "\r\n" +
                    "Transport: RTP/AVP;unicast;client_port=49501-49502" "\r\n"
            )
            request = self.authorization(request, self.config)
            request += "\r\n"
            self.s.sendall(request.encode())
            response = self.s.recv(4096).decode()
            print(f'RESPONSE {response}')
            print(request)
            if "RTSP/1.0 200 OK" not in response:
                return False

            response_lines = response.split("\r\n")
            for line in response_lines:
                if line.startswith("Session:"):
                    self.session = line.split("Session:")[1].strip()

            return True

    def send_play_request(self):
        if self.send_setup_request():
            request = (
                    "PLAY " + self.config['url'] + " RTSP/1.0\r\n" +
                    "CSeq: 4"  "\r\n" +
                    "Session: " + str(self.session) + "\r\n" +
                    "User-Agent: Python MJPEG Client"  "\r\n" +
                    "Range: npt=0.000-\r\n"
            )
            request = self.authorization(request, self.config)
            request += "\r\n"
            self.s.sendall(request.encode())
            response = self.s.recv(4096).decode()
            print(response)
            self.s.close()
            if "RTSP/1.0 200 OK" in response:
                return True
            return False

